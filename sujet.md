# Avancées et réalités de l'IA
## Comprendre les challenges actuels en appliquant les avancées actuelles sur un cas concret de A à Z.



L'objectif de ce cours est d'appronfondir vos connaissances en sciences des données et en IA au travers de la mise en place d'un système de questions/réponses (Question Answering) francophone sur un corpus spécifique à une thématique donnée.

Le question answering (QA) qui consiste, étant donné une question formulée en langue naturelle,  à produire une réponse (en langue naturelle) à cette question est un challenge important de l'IA et du NLP aujourd'hui connu sous le nom de [*machine reading comprehension*](https://project.inria.fr/paiss/files/2018/07/perez-machine-reading.pdf).


Le traitement du langage naturel basé sur le Deep Learning est un domaine qui évolue très rapidement à l’heure actuelle. Plus récemment, la tâche de Language Modelling avec des modèles à l’état de l’art tels que  BERT, GPT-2 ou XLNet ont permis de faire des avancées impressionnantes dans la majorité des tâches du NLP. Comme ces modèles peuvent être entraînés sur du texte brut, les possibilités de s'attaquer à de nouveaux problèmes spécifiques à l'industrie sont innombrables.


Les enjeux sont les suivants: 

 +  Comprendre les modèles de langage à l’état de l’art du NLP (BERT, Roberta, ALBERT, etc.)
 +  Comprendre une tâche complexe d'IA telle que le Question Answering. 
 +  Comprendre et appliquer le paradigme de Transfer Learning (transfert vers une autre langue, transfert à une tâche spécifique)
 +  Éprouver les difficultés d'un projet d'IA "réel" liées : 
 	+  Données massives mais non annotées
 	+  Difficulté d'acquisition d'annotation de bonne qualité
 	+  Nettoyage et traitement des données
 	+  Présence et prise en compte du biais
 	+  Interprétabilité



## Contenu

L'idée du projet est de proposer un démonstrateur Question Answering francophone sur un corpus spécifique à une thématique donnée (exemple: écologie, santé, éducation, etc.).

Plusieurs étapes sont proposées au cours du projet.


### Phase 1 : Intro et prise en main. Reproduction d'un modèle de QA générique (2 séances de cours)

+ Comprendre la problématique du Question Answering
+ Etudier et comprendre les avancées en modelisation du language et notamment les modèles pré-entrainés de type BERT (Transformers)
+ Prise en main des outils : Python / PyTorch, HuggingFace .	
Lors de cette phase, il s'agira de :

 + Entraîner un modèle de QA standard en anglais (SQuAD 1.1) pour comprendre l'état de l'art. L'objectif est de réussir un entraînement et reproduire les performances.
 + Transférer en français sur un dataset fourni par ILLUIN.



### Ressources pour la phase 1 

#### Question Answering
+ Review : [ici](https://reader.elsevier.com/reader/sd/pii/S131915781830082X?token=512545C83616B39F34540B7B73CA10890501BAA72CADB2BF304D8EC6FBED8DE7D8390D9029EB2580A081DC49D0AF6D59)
+ Tutorial : [ici](https://project.inria.fr/paiss/files/2018/07/perez-machine-reading.pdf)
+ Machine reading Comprehension review : [ici](https://arxiv.org/pdf/1907.01686.pdf)


#### Languages models

+ De très bons tutoriels ou blogs :
	+ Présentation de BERT très pédagogique [ici](https://lesdieuxducode.com/blog/2019/4/bert--le-transformer-model-qui-sentraine-et-qui-represente)
	+ Présentation plus succinte du principe des modèles de langues pré-entrainés : [ici](https://weave.eu/deep-transfer-learning-nlp-revolution/).
	+ Explication illustrée  des transformers et cie : [ici](http://jalammar.github.io/illustrated-transformer/)

+ Les articles de références :
	+	 Attention Is All You Need, Vaswani, N. Shazeer, N. Parmar, J. Uszkoreit, L. Jones, A. N. Gomez, L. Kaiser, I. Polosukhin : [ici](https://arxiv.org/abs/1706.03762)
	+	BERT: Pre-training of Deep Bidirectional Transformers for Language Understanding, Devlin, M.-W. Chang, K. L., K. Toutanova, [ici](https://arxiv.org/abs/1810.04805)
   
#### Les outils, depôts utiles 

 + HuggingFace, Transformers : [ici](https://github.com/huggingface/transformers) ou plus généralement [là](https://huggingface.co/)
 + [Pytorch](https://pytorch.org/) 
 + [SQuAD dataset](https://rajpurkar.github.io/SQuAD-explorer/explore/1.1/dev/) et article [ici](https://arxiv.org/pdf/1606.05250.pdf)
 + Pour une première chaîne de traitements : un tuto [ici](https://towardsdatascience.com/question-answering-with-bert-xlnet-xlm-and-distilbert-using-simple-transformers-4d8785ee762a)


En début de séance 2, il faudra par groupe présenter de manière succinte des élements de réponses aux questions suivantes :


 1. Principe du mécanisme d'attention dans les réseaux recurrents.
 2. Présentation de BERT et de l'approche transformers.
 3. BERT versus ses concurrents (e.g. [ici](https://towardsdatascience.com/pre-trained-language-models-simplified-b8ec80c62217))
 4. ALBERT : article [ici](https://arxiv.org/abs/1909.11942)
 5. Modèle état de l'art en Question Answering, architecture.

 



 